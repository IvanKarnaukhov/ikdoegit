/**
 * Задача 1.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо преобразовать первый символ переданной строки в заглавный.
 *
 * Условия:
 * - Необходимо проверить что параметр str является строкой
 */

function upperCaseFirst(str) {
    if (typeof str !== 'string') {
        return '';
    } else {
        return str.charAt(0).toUpperCase() + str.slice(1)
    }
}

console.log(upperCaseFirst('petter')); // Petter
console.log(upperCaseFirst('')); // ''
