/**
 * Задача 1.
 *
 * Создайте свойство `salary` в объекте `person`.
 * При чтении этого свойства должна возвращаться строка с текстом.
 * Возвращаемая стрка должна содержать текст: `Зарплата за проект составляет 100$`
 * Где 100 это произведение ставки в час `rate` на количество отработанных часов `hours`
 *
 * Создайте свойство `rate` в объекте `person`.
 * Свойство `rate` можно менять, но нельзя удалять.
 * Свойство `rate` должно содержать число.
 *
 * Создайте свойство `hours` в объекте `person`.
 * Свойство `hours` можно менять, но нельзя удалять.
 * Свойство `hours` должно содержать число.
 *
 * Условия:
 * - Свойство salary обязательно должно быть геттером.
 *
 * Обратите внимание!
 * - Для того что бы обратиться к свойству оъекта необходимо использовать this.hours и this.rate
 * - Для решения данного задания вам потребуется defineProperty или defineProperties
 */


//
// const person = {
//     hours: 2.5,
//     rate: 100,
//     text: 'Зарплата за проект составляет',
//     // set salary() {
//     //     if (this.rate === 'string') {
//     //         throw new Error('Rate should be a number')
//     //     }
//     //
//     // },
//     //get salary() {
//         return `${this.text} ${this.rate}$`;
//     },
// };
//
// console.log(person.salary); // `Зарплата за проект составляет ххх$`

/**
 тут я подзавис и решил решить по другому
 */

// const rate = {};


const person = {
    textSalary: 'Зарплата за проект состовляет',
    get salary() {
        return `${this.textSalary} ${this.rate * this.hours}$ `
    }
};

Object.defineProperty(person, 'rate', {
    value: 40,
    configurable: false
});

Object.defineProperty(person, 'hours', {
    value: 2.5,
    configurable: false
});

console.log(person.salary); // `Зарплата за проект составляет ххх$`
