/**
 * Задача 2.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо присвоить переменной result значение true,
 * если строка source содержит подстроку spam. Иначе — false.
 *
 * Условия:
 * - Необходимо выполнить проверку что source и spam являются типом string.
 * - Строки должны быть не чувствительными к регистру
 */

function checkSpam(source, spam) {
    if (typeof source === 'string' && typeof spam === 'string') {
        return source.toLowerCase().includes(spam.toLowerCase())
            }
    return 'its not a string'
}

console.log(checkSpam('pitterXXX@gmail.com', 'xxx')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'XXX')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'sss')); // false
console.log(checkSpam(1, "1")); // its not a string


/**
 *
 * Я добавил, что выйдет в консоль если придет не строка
 *
*/